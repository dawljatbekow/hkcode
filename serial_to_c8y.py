#! /usr/bin/env python3
# -*- coding: utf-8 -*-

import csv
import http.client
import os
import serial
import time

id = '110511'
ser = serial.Serial('/dev/ttyUSB1', 115200)

time.sleep(5)
ser.readline()

conn = http.client.HTTPSConnection("greencitysolutions.cumulocity.com")

headers = {
    'accept': "application/vnd.com.nsn.cumulocity.measurement+json",
    'content-type': "application/json",
    'authorization': "Basic cy5kYXdAbXlnY3MuZGU6MUtub3dteXB3",
    'cache-control': "no-cache",
    }

while 1 :
    time.sleep(1)
    d = ser.readline().decode().split()
    dt='20'+(d[1][0:2])+'-'+(d[1][3:5])+'-'+(d[1][6:8])+'T'+(d[2])
    payload = "{\n\t\"gcs_libelium_PowerLevelMeasurement\": {\n      \"E\": { \n          \"value\": "+d[10]+",\n            \"unit\": \"%\" }\n        },\n  \"gcs_libelium_AirPolutionPM1.0Measurement\": {\n      \"PM1.0\": { \n          \"value\": "+d[3]+",\n            \"unit\": \"ug/m3\" }\n        },\n    \"gcs_libelium_AirPolutionPM2.5Measurement\": {\n      \"PM2.5\": { \n          \"value\": "+d[4]+",\n            \"unit\": \"ug/m3\" }\n        },\n    \"gcs_libelium_AirPolutionPM10Measurement\": {\n      \"PM10\": { \n          \"value\": "+d[5]+",\n            \"unit\": \"ug/m3\" }\n        },\n    \"gcs_libelium_AirPolutionNO2Measurement\": {\n      \"NO2\": { \n          \"value\": "+d[6]+",\n            \"unit\": \"ppm\" }\n        },\n    \"gcs_libelium_AirTemperatureMeasurement\": {\n      \"T\": { \n          \"value\": "+d[7]+",\n            \"unit\": \"C\" }\n        },\n    \"gcs_libelium_AirHumidityMeasurement\": {\n      \"h\": { \n          \"value\": "+d[8]+",\n            \"unit\": \"%\" }\n        },\n    \"gcs_libelium_AirPressureMeasurement\": {\n      \"P\": { \n          \"value\": "+d[9]+",\n            \"unit\": \"Pa\" }\n        },\n    \"time\":\""+dt+"\", \n    \"source\": {\n      \"id\":\""+id+"\" }, \n    \"type\": \"gcs_Meassurement\"\n}"
    conn.request("POST", "/measurement/measurements", payload, headers)
    res = conn.getresponse()
    data = res.read()
    #print(data.decode("utf-8"))
    time.sleep(30)